# Senior Bioinformatician Pilot Project: Potato Genome Updates Dashboard

This pilot project is designed to evaluate your skills in bioinformatics, particularly in processing next-generation sequencing (NGS) data, and your ability to develop a Django-based web application. The goal of this project is to create an application that keeps users updated on newly released potato genome sequences. You are expected to complete the project within 48 hours. Your code should be well-documented, structured, and available on a public GitHub repository. Please submit the link to your repository once the project is completed.

## Project Overview

Create a Django web application to fetch and display the latest potato genome sequences from the SRA Run Selector, filtering for genomes published after 2018. The application should have a dashboard where users can view the data in a flexible framework such as DataTables. The pipeline should automatically update the data at a configurable interval, and if a scientific paper or preprint is available that is linked to the dataset, it should be included.

### Requirements

1. Use Python 3.x and Django for web application development.
2. Implement a flexible and user-friendly interface for displaying the results using DataTables or a similar framework.
3. Fetch the latest potato genome sequences from the SRA Run Selector, filtering for genomes published after 2018.
4. Automatically update the data at a configurable interval.
5. Include linked scientific papers or preprints, if available.
6. Write well-documented and structured code, following best practices in software development.

### Instructions

1. Set up a new Django project and create an app for the potato genome updates dashboard.
2. Implement an interface for displaying the fetched potato genome data using a flexible framework like DataTables.
3. Develop a pipeline to fetch the latest potato genome sequences from the SRA Run Selector, filtering for genomes published after 2018. You may use Python libraries like BeautifulSoup or requests for web scraping.
4. Implement a mechanism to automatically update the data at a configurable interval (e.g., daily, weekly, monthly).
5. If a scientific paper or preprint is available and linked to the dataset, include it in the displayed data.
6. Ensure that your code is well-documented with comments and follows a consistent style. You may use tools like pylint or flake8 to enforce code quality.
7. Write a README.md file that includes instructions on how to set up and run your application, as well as a brief explanation of your data fetching pipeline and the technologies used.
8. Commit your code to a public GitHub repository and submit the link.

### Evaluation Criteria

Your project will be evaluated based on the following criteria:

- Functionality: The application should work as described and display the latest potato genome sequences accurately.
- Code quality: Your code should be well-documented, structured, and follow best practices in software development.
- User interface: The dashboard should be user-friendly and visually appealing.
- README: Your README file should provide clear instructions for setting up and running the application and explain the data fetching pipeline and technologies used.

Good luck with the pilot project! We're looking forward to reviewing your submission.
